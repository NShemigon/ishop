var express = require('express');
var router = express.Router();
var Product = require('../models/product');

/* POST create new product, return product */
router.post('/', function(req, res, next) {
    var body = req.body;

    Product.create(body, function (err, product) {
        if (err) {
            return next(err);
        }

        res.send(product);
    });
});

/* GET all products */
router.get('/', function(req, res, next) {
    //:TODO create pagination

    Product.find(function (err, products) {
        if (err) {
            return next(err);
        }

        res.send(products);
    });
});

/* PUT products */
router.put('/', function(req, res, next) {
    res.send('Product require update by id');
});

/* DELETE all products */
router.delete('/', function(req, res, next) {
    Product.remove({}, function (err, result) {
        if (err) {
            return next(err);
        }

        res.send(result);
    });
});

/* POST create new product by id, return message */
router.post('/:id', function(req, res, next) {
    var id = req.params.id;

    res.send('Product with id ' + id + ' already exists');
});

/* GET product by id */
router.get('/:id', function(req, res, next) {
    var id = req.params.id;

    Product.findById(id, function (err, product) {
        if(!product) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        if (!err) {
            return res.send({ status: 'OK', product: product });
            //return res.send(product);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

/* PUT change product by id */
router.put('/:id', function(req, res, next) {
    var id = req.params.id,
        body = req.body,
        options = {"new": true};

    Product.findByIdAndUpdate(id, body, options, function (err, product) {
        if (err) {
            return next(err);
        }

        res.json(product);
    });
});

/* DELETE product by id */
router.delete('/:id', function(req, res, next) {
    var id = req.params.id;

    Product.findOneAndRemove(id, function (err, product) {
        if (err) {
            return next(err);
        }

        res.json(product);
    });
});

module.exports = router;

