angular.module('app').controller('ProductController', [
    '$scope',
    '$resource',
    function ($scope, $resource) {
        var Product = $resource('/products/');
        var ProductId = $resource('/products/:id');

        refreshProducts();

        $scope.save = function () {
            var product = Product.save($scope.product, function() {
                refreshProducts();
                $scope.product = {};
            });
        };

        $scope.del = function() {
            var productId = ProductId.remove({__id: "@id"}, function() {
                refreshProducts();
            });
        };

        function refreshProducts() {
            $scope.products = Product.query();
        }
    }
]);
