var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema( {
    name: { type: String, required: true },
    description: { type: String }
} );

module.exports = mongoose.model('Group', schema);