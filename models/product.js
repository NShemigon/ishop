var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema( {
    name: { type: String, required: true },
    description: { type: String },
    price: { type: Number }
} );

module.exports = mongoose.model('Product', schema);
