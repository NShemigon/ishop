module.exports = function(grunt) {

    grunt.initConfig({
        //pkg: grunt.file.readJSON('package.json'),
        bowercopy: {
            options: {
                srcPrefix: 'bower_components'
            },
            libs: {
                options: {
                    destPrefix: 'public/libs'
                },
                files: {
                    // Angular JS
                    'angular.min.js': 'angular/angular.min.js',

                    // Angular Resource
                    'angular-resource.min.js': 'angular-resource/angular-resource.min.js',

                    // Angular Route
                    'angular-route.min.js': 'angular-route/angular-route.min.js'
                }
            }
        },
        zip_directories: {
            myzip: {
                files: [{
                    filter: 'isDirectory',
                    expand: true,
                    cwd: './',
                    src: ['public'], // how directories
                    dest: './myzip'
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-zip-directories');

    grunt.registerTask('run', ['bowercopy']);
    grunt.registerTask('zip', ['zip_directories']);
};